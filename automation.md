Legend:
* {name} refers to particular ID or type/face/turn/group name
* `name` refers to a particular value/option/parameter

# Automation
All operations change the game state (like an API), even the act of collecting Components. The resulting "Collection" represents an ad-hoc Stack in Limbo. You can think of it as temporarily taking something into your hand in order to move/flip/rotate/shuffle it, etc.

## Operations
* __collect__
    * collect `amount` (of) Components from `top`/`bottom` of {stack/holder}
    * collect all Components from {holder}
    * collect all Components from `position` (x,y) of {holder}
    * collect all Components (of `group` as optional parameter) from {stack/holder}
    * collect all Components of `type` (and `group` as optional parameter) from {surface/hand/Limbo}
    * collect all Components of `type` (and `group` as optional parameter) from all `surfaces` or `hands` or `both`
    * collect all Components of `type` (and `group` as optional parameter) from all spaces
* __move__
    * move Collection to `position` (z) of {stack}
    * move Collection to `top`/`bottom` of {stack}
    * move Collection to {holder}
    * move Collection to `position` (x, y) of {holder}
    * move Collection to `position` (x, y, z) of {surface/hand}
* __flip__ (faces)
    * flip Collection forward to next face
    * flip Collection backwards to previous face
    * flip Collection to random face
    * flip Collection to face {face name}
* __turn__ (cycle)
    * turn Collection forward to next cycle
    * turn Collection backward to previous cycle
    * turn Collection to random cycle
    * turn Collection to cycle {turn name}
* __spin__ (Spinner)
    * spin {spinner}
* __change__ (Counter)
    * set {counter} to `value`
    * change {counter} by `value` (set negative value to decrease, use "*" or "/" to multiply or divide)
* __reorder__
    * reverse order of Collection
    * shuffle Collection
    * reset order of Collection (to deck definition)
* __distribute__
    * distribute `amount` (of) Components from `top`/`bottom` of Collection to {hand name} of all players
    * distribute Collection to {hand name} of all players
    * distribute Collection to {hand name} of all players until indivisible rest remains
* __count__
    * count Components in {stack} as {counter}
    * count Components in `position` (x, y) of {holder} as {counter}
    * count Components in {surface/hand/Limbo} as {counter}
    * count Players in room as {counter}
* __redefine__ (Holder properties)
    * set `property` of {holder} to `value`
    * toggle `property` of {holder} (for boolean properties only)
    * Properties:
        * stackDirection
        * isTotalDisplayed?
        * isFlippable?
        * FlipOnEnter
        * FlipOnLeave
        * isTurnable?
        * TurnOnEnter
        * TurnOnLeave
        * isClickable?
        * isDraggable?

# Automation Examples
From PCIO: "Move 4 cards from {sourceHolder} to {targetHolder}, and flip face up"
* collect 4 Components from `top` of {sourceHolder}
* flip Collection to face `up`
* move Collection to {targetHolder}
---
From PCIO: "Switch top card only face up/down from {holder}"
* collect 1 Component from `top` of {holder}
* flip Collection to next face (or cycle back to inital face)
* move Collection to {holder}
---
From PCIO: "Shuffle cards from {holder1} and {holder2}"
* collect all Components from {holder1}
* shuffle Collection
* move Collection to {holder1}
* collect all Components from {holder2}
* shuffle Collection
* move Collection to {holder2}
---
From PCIO: "Decrease {counter1}, {counter2}, {counter3} by 5"
* change {counter1} by -5
* change {counter2} by -5
* change {counter3} by -5
---
A fairly typical "shuffle and then deal the entire deck equally to all players and remove any remaining cards from game" case (e.g. dealing 52 cards to 5 players):
* collect all Components from {holder}
* shuffle Collection
* distribute Collection to {hand name} of all players until indivisible rest remains
* move Collection to {limbo}
