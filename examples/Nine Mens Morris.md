# Nine Men's Morris
This two-player game uses a board and 18 movable pieces (9 per player). For the sake of demonstrating Holders and Stacks, we'll provide several setup variants.

## Common to all setups
Definitions:
* Component
    * type: "board"
    * label: none
    * width and height: (some measure)
    * border: none
    * flip properties:
        * faces:
            * face #1:
                * name: "up"
                * image: (some image URL)
                * isEnlargable: false
    * turn properties: none

## Setup 1. All pieces are individually placed next to the board.
Definitions:
* 

## Setup 2. There pieces are placed in a Stack (one for each player).

## Setup 3. The pieces are placed in a 3x3 Holder (one for each player).



Defaults:
* position: (x,y,z)
* side: 1
* rotation: 1
* containedComponents: null (0 for Holders)
* selectedComponents: null (0 for Holders)
* isVisible: false
* isClickable: false
* isDraggable: false
* isDragged: false
* isRemoved: false

GOM:
* "surface1"
    * "board1" (position:xy, isVisible)
    * "holder1" (position:xy, isVisible)
        * "stack1" (position:xy, isVisible, isClickable, containedComponents:9)
            * "component1"
            * "component2"
            * "component3"
            * "component4"
            * "component5"
            * "component6"
            * "component7"
            * "component8"
            * "component9"
    * "holder2"
        * "stack2"
            * "component10"
            * "component11"
            * "component12"
            * "component13"
            * "component14"
            * "component15"
            * "component16"
            * "component17"
            * "component18"

Shapes:
* "surface1"
    * "board1"
    * "holder1" (position:xy, isVisible)
        * "stack1" (position:xy, isVisible, isClickable)
            * "component1" ()
            * "component2"
            * "component3"
    * "holder1"
        * outline
        * container:
