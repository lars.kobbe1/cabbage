# Checkers

## CBBG Format

### Definition

## PCIO Format
```
[
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 1196.16,
    "y": 767.34,
    "z": 155,
    "id": "0f32b648-b02f-4121-9d46-b5e7134dacb4",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 446.16,
    "y": 13.52,
    "z": 137,
    "id": "b22bed5c-6c3e-4ec7-8c16-7b9929d31df3",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 946.93,
    "y": 266.57,
    "z": 148,
    "id": "f18d97b0-d9a1-4fdb-aacf-51bf2e203e60",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 1070.78,
    "y": 895.78,
    "z": 133,
    "id": "81f023d0-0b78-4c96-bb48-994754c37eed",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 697.69,
    "y": 766.57,
    "z": 153,
    "id": "1ba7f102-7c4b-4611-9550-ac314c1ecc64",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 695.4,
    "y": 265.81,
    "z": 142,
    "id": "ed6fdd22-c832-468c-a571-ea207b449267",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 446.93,
    "y": 765.81,
    "z": 129,
    "id": "eb267f73-0f70-4b1c-943d-cac85527c2a3",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 820.78,
    "y": 894.25,
    "z": 157,
    "id": "65ba1f06-420d-4b23-9434-e15af1962b95",
    "dragging": null
  },
  {
    "type": "spinner",
    "options": [
      "T",
      "H"
    ],
    "x": 1427.05,
    "y": 804.04,
    "z": 163,
    "rotation": 2144,
    "value": "H",
    "id": "66ab6a79-3667-4cd6-bb51-a88f3ffc629a",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 1196.93,
    "y": 14.28,
    "z": 146,
    "id": "9aa217df-ae40-4f15-be7d-e40e7f0be093",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 696.16,
    "y": 14.28,
    "z": 140,
    "id": "ad68db95-bb4d-4450-bdcc-9c7de29add24",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 321.54,
    "y": 896.24,
    "z": 156,
    "id": "f757f2e2-afd2-43c6-a337-8c17c979ffa8",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 571.54,
    "y": 641.19,
    "z": 159,
    "kinged": false,
    "id": "247dba18-18c9-4290-9e52-16512b18f4f6",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 822.31,
    "y": 142.72,
    "z": 141,
    "id": "df5048c0-9cb2-4a1b-b09c-f8ff512c878d",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 570.02,
    "y": 140.43,
    "z": 149,
    "id": "25578338-fb01-4b5f-a127-ae29e68d4016",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 1196.93,
    "y": 265.81,
    "z": 147,
    "id": "f5404648-1022-4e33-ab5d-298b89ed87b5",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 1071.54,
    "y": 641.96,
    "z": 152,
    "id": "20cff9f9-9367-4abb-823c-501fd137980a",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 1071.54,
    "y": 140.43,
    "z": 145,
    "id": "da1db7a7-9bcb-4dca-876a-6506c9f74b5a",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 444.63,
    "y": 267.34,
    "z": 160,
    "kinged": false,
    "id": "22ef8533-a4e4-470a-8bb6-4e32a3384d56",
    "dragging": null
  },
  {
    "id": "hand",
    "type": "hand",
    "x": 50,
    "y": 820,
    "z": 1,
    "enabled": false,
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 947.69,
    "y": 766.57,
    "z": 154,
    "id": "cf7067a0-2e4d-45b3-b9b7-ec6de91eba46",
    "dragging": null
  },
  {
    "type": "board",
    "x": 303.2,
    "y": 0,
    "z": 67,
    "id": "e3a1e406-b84f-4c5f-8096-6fee50c38c8c",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 946.16,
    "y": 14.28,
    "z": 144,
    "id": "f0ff1f81-1dcc-441d-9e60-db9e81846e3a",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 319.25,
    "y": 642.72,
    "z": 150,
    "id": "3632b517-1d7a-4862-9c20-59f961acd960",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 570.02,
    "y": 895.02,
    "z": 131,
    "id": "b66510d9-b2dd-4e48-ae1e-7e9b5e97f0f3",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "black",
    "x": 820.02,
    "y": 641.19,
    "z": 151,
    "id": "be79248f-394b-41c3-90e5-231dba30210f",
    "dragging": null
  },
  {
    "type": "gamePiece",
    "pieceType": "checkers",
    "color": "red",
    "x": 319.25,
    "y": 139.66,
    "z": 136,
    "id": "49a824f4-1602-4e82-a2ac-b471e32d9c4d",
    "dragging": null
  }
]
```
