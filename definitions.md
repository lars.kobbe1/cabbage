# Definitions

Definitions are loaded when importing a Game Module. They cannot be changed.

## Component Properties
* ID
* Name. This is a custom name like "Ace of Spades". It is not displayed anywhere but could be used for scripting purposes.
* Type. This is a custom name like "Big Card", "d6" or the like. Components of the same Type are considered to be stackable.
* Grouping. These are custom key-value pairs like "Deck = Red" or "Value = 13". The Grouping can be used in Automation as a means to target only particular Components.
* Label. If set, the Component is shown with a Label next to it.
    * Title
    * Offset: x, y
    * isLabelEditable?
* Width & Height (px).
* Layer. Layers determine which Components always go above others from another layer. In most games, only four layers are needed (0 for the board, 1 for Labels, 2 for Holders, 3 for Automation Buttons, 4 for cards and 5 for tokens that could be placed on cards). 
* Border (round, sharp or none). Determines whether the Component should be outlined with a border and whether it should have rounded edges or not.
---
* Faces: List of Faces/Sides.  Boards have one side, cards have two faces and typical dice have six faces. Each face is defined by
    * Face Name (optional). This can simply be a number (1 through 6) or some descriptive title like "face-up" and "face-down". These names may be displayed to the player for selection.
    * Face Image. References an asset (image file) in the Game Module.
    * Zoom Level (optional). Cards with minute details should typically be enlarged on mouse hover. Nevertheless, if the face is just showing a red backside, then there's no sense in enlarging it.
---
* Turns: List of Turns. Cards typically have four turns, but for game like MtG where cards can only turn sideways, only two turns are needed. Every Component has at least one Turn (at 0 degrees). Each turn is defined by
    * Turn Name. This can simply be a number (1 through 6) or some descriptive title like "up" and "tapped". These names may be displayed to the player for selection.
    * Turn rotation degree (0-359)
---
* Action Properties:
    * Click Action. References an action ID. All Components of a particular type have the same action on click.
    * Long Click Action. A list of action IDs (can be a single entry). All Components of a particular type have the same action list on long click.

## Stack Properties:
* ID
* Type: "Stack"

## Holder Properties:
* ID
* Type: "Holder"
* Name. This is a custom name like "Discard Pile" or "Blue Deck". It is not displayed anywhere but could be used for scripting purposes.
* Label. If set, the Holder is shown with a Label next to it. Labels are non-interactive (click/drag).
    * Title
    * Offset: x, y
    * isLabelEditable
* Grid form. Either "static" or "dynamic". A static grid has a pre-defined amount of rows and columns. Adding further Components to the Holder may add additional rows or columns (based on grid direction).
* Grid columns (for "static" grids). The number of columns in the grid.
* Grid rows (for "static" grids). The number of rows in the grid.
* Grid direction (for "dynamic" grids). Either "row" or "column". Determines whether the grid extends in rows or columnms when new Components are added.
* Drop-zone width and height. The width and height for each drop-zone in the Holder.
* Drop-zone border (round, sharp or none). Determines whether the drop-zones should be outlined with a border and whether they should have rounded edges or not.
* Drop-zone offset: Odd and even rows/columns (based on Grid direction) can have different offsets. The definition of Even offsets is optional.
    * Odd x, y, rotation. The offset (or overlap) between each drop-zone and the rotation for odd rows/columns.
    * Even x, y, rotation. The offset (or overlap) between each drop-zone and the rotation for odd rows/columns.
* stackDirection. Either "top" or "bottom". Determines where Components of same type will be added.
* isTotalDisplayed. Applies to content of Holder (Stacks in this case).
* isFlippable? Applies to content of Holder.
* FlipOnEnter: Either "keep", "toggle" or specific face.
* FlipOnLeave: Either "keep", "toggle" or specific face.
* isTurnable? Applies to content of Holder.
* TurnOnEnter: Either "keep", "toggle" or specific turn.
* TurnOnLeave: Either "keep", "toggle" or specific turn.
* isRearrangable? In "static" grids, this allows to place a Component to another free position in the Holder. In "dynamic" grids, Components can be inserted "between" other Components (i.e. moving everything in stack direction one further place in the grid direction).
* isClickable? Applies to content of Holder.
* isDraggable? Applies to content of Holder. Only restricts dragging Components out of the Holder.
