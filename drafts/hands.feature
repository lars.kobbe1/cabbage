Feature: Hands
    Hands contain a Component Holder of type "Spread" or "Pile". Each player have the same Hand (or multiple Hands).

    Properties:
    * unique id (only for internal purposes)
    * Hand name (same name for this Hand for each player)
    * type: "Spread" or "Pile"
        * id of Spread or Pile
    * x,y position (relative to Screen)
    * width and height
    * label
        * text
        * x offset from top-left corner
        * y offset from top-left corner
    Abilities/Controls:
    * show outline? (# TODO: clarify if we need styles or even images for Hands)
