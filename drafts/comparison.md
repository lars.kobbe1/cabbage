Card Deck:
- static or dynamic face side

- one or more cards, each with
    - face-up image, with
        - x,y pos
        - width & height
        - custom image layer(s)
        - custom text layer(s)
    - face-down image, with
        - custom image layer(s)
        - custom text layer(s)
    - number of copies
- overlap: normal or no overlap
- when moving card out of hand: flip face-up or do nothing
- "are you sure" prompts:
    - recall from table
    - recall from table and hands
- enlarge on hover/long press
