Feature: Avatars
    Avatars represent the Players and their Hands (as they are invisible to everyone but the owning player).

    Rule: Players may pick a (unique) icon to represent themselves as Avatars.
        Example: A very simple Avatar would be a colored circle.
    
    Rule: Avatars have a Tag next to their icon, which can show a particular number.
        Example: In most games, the number represents the amount of cards in their Hand.
        Example: The tag may show "?" if no number should be displayed.
    
    Rule: Avatar icons cannot be moved, rotated, flipped or otherwise changed.
        Example: In most games, you would want Avatars to be in fixed positions, just like Boards.