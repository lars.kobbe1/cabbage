Feature: Components
    General Properties:
    * type id 
    * width and height
    * list of faces, for each
        * name or number (for flip selection)
        * can be enlarged?
            * width & height for enlarged display
    * show border?
        * if yes, with rounded or hard edge
    * show name as label?
        * label position (x,y offset from top-left corner)
    * can be moved? (by dragging & dropping)
    * can be flipped? (ignored for single-faced Components)
        * if yes, flips to a) next/other face or b) random face or c) choice of face
    * can be rotated?
        * if yes, rotates to a) next/other turn or b) random turn or c) choice of turn
    * can be spun? (shows a pointer that rotates, ignored for single-faced Components)
    * can be removed? (from the game)
    * can be enlarged?
    * can be stacked?
        * stack template id (to be applied for ad-hoc stacks)
    * action on click: flip to next face (or custom)
    * actions on long click: list of actions based on enabled abilities above (or custom)

    Individual Properties:
    * individual id
    * name (to be used for label display)
    * faces, ordered list, each with 
        * image ID (see below)
        * can be enlarged? (on hover)
    * default face
    * flip direction (increasing/decreasing)
    * rotation turns, ordered list, each with
        * name (for choice of turns)
        * degrees of clockwise rotation (e.g. 90, 180, 270)
    * default rotation
    * turn direction (clockwise/anti-clockwise)
    * stack direction (top/bottom)

    State Properties:
    * current position
    * current width and height
    * current visibility state
    * current clickable state
    * current draggable state
    * current face
    * current rotation turn
    * current stack ID (empty if not stacked)
    * isDragged? (temporary state while dragging)
    * isRemoved? (state before being in play or after being in play)

    Image: (following PCio properties, these properties are simply translated into a SVG)
    * image ID
    * background color (can be transparent)
    * text layer(s)
        * x,y offset
        * width & height
        * text value
        * font family, size and alignment
    * image layer(s)
        * x,y offset
        * width & height
        * image URL

    Rule: Components may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Card moves it.

    Rule: Components may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the Card to the next rotation turn.
    
    Rule: Components may be spun unless restricted.
        Example: Clicking a Spinner rotates a pointer to a random turn (or face?).

    Rule: Components may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Card from the game.

    Rule: Moving a Component onto another Component of the same type creates a Component Stack unless restricted.
        Example: Dragging & dropping a Component of type "Big Card" on another Component of type "Big Card" makes them stack together.
        # TODO: confacer if we need abilities to restrict Components of same type but different Component Group from stacking or if that should be handled via automation only.

    Rule: Components may be flipped to the next/other face unless restricted.
        Example: Clicking a face-down Card flips it face-up.
        Example: Clicking a Die flips it to the next face.
    
    Rule: Components may be tossed to a random face unless restricted.
        Example: Long-Clicking and selecting "Toss" flips the Card to a random face.

    Rule: Components may be enlarged. The faces to be enlarged can be defined.
        Example: Hovering over a face-up Card shows an enlarged version of the Card.
