Feature: Stack Templates
    
    Componenent Stacks contain Components of the same type. They can be formed ad-hoc by dropping a matching Component on bottom of another Component. Therefore, their properties and abilities are defined in a Component's Stack Template. However, all default properties and abilities can be overriden for stacks when they placed inside a Component Holder.

    Properties:
    * stack template id (to be referenced in Components and Component Holders)
    * container rotation: "keep" (for ad-hoc stacks)
    * container display side: "keep" (for ad-hoc stacks)
    * alignment of stacked Components:
        * x offset from bottom-left corner (in px or percentage?)
        * y offset from bottom-left corner (in px or percentage?)
    * amount of stacked Components to display (defaults to 3 if not defined)
    * relative position of displayed Components:
        * x offset from bottom-left corner (in px or percentage?)
        * y offset from bottom-left corner (in px or percentage?)
        * z offset of added Components: either "above" or "below"
    Abilities/Controls:
    * shows total in Tag? (if hidden, show "?")
    * can be rotated (as a whole)
    * can be turned over? (flip and re-order)
    * can be shuffled?
    * can be drawn/selected from?
    * Top-most Component can be tossed/flipped/re-rerolled?
    * Top-most Component can be rotated?
    * flip Components when they are added to the Stack?
        * when flipped, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are added to the Stack?
        * when rotated, rotates to a) next/other segment or b) random segment or c) choice of segment
    * flip Components when they are moved out of the Stack?
        * when flipped, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are moved out of the Stack?
        * when rotated, rotates to a) next/other segment or b) random segment or c) choice of segment

    Rule: Component Stacks always restrict the type of Components that can be stacked.
        Example: A Component of type A cannot be stacked with a Component of type B.
        Example: A Component with two sides cannot be stacked with a Component with six sides.
        Example: A 200x300 px Component cannot be stacked with a 10x10 px Component.
    
    Rule: Components may be moved in bulk when dragging and dropping their Component Stack Tag.
        Example: A Component Stack with 5 Components is moved by using the "5" Tag instead of the bottom-most Component.

    Rule: Component Stacks show the number of contained Components unless the total should be hidden.
        Example: When no display is desired, the Tag shows "?".

    Rule: Selected Components may be moved together when dragging and dropping their Tag.
        Example: A Component Stack with 2 out of 5 Components selected is moved by using the "2/5" Tag instead of the bottom-most Component.

    Rule: The stacking order of Components is set to either "above" or "below" the other Components.
        Example: When stacking order is set to "above", added Components are placed above the previous Component in the stack.
    
    Rule: The amount of visibly stacked Components can be defined.
        Example: When only 3 Components are defined to be visible, adding a fourth Component changes the Tag count, but not the visible presentation of stacked Components.
    
    Rule: The overlap of Components in the Component Stack can be defined.
        #TODO: clarify where this is defined

    Rule: Components may be selected individually or in bulk. By default, Components are unselected.
        Example: Clicking the Component Stack increases the selected count in the Tag.

    Rule: Component Stacks show the number of selected Components.
        Example: When two out of 5 Components are selected, the Tag shows "2/5".
        Example: When no total display is desired, the Tag shows "2/?".
    
    Rule: The Component selection can be undone in a single step.
        Example: Long-Clicking the Tag unselects all Components.

    Rule: The top-most Component may be flipped unless restricted.
        Example: Clicking the top-most card flips a face-down Card to its face-up side.

    Rule: Component Stacks may be flipped unless restricted.
        Example: Long-Clicking and selecting "Flip" flips all Cards in the Card Stack

    Rule: Component Stacks may be turned over unless restricted.
        Example: Long-Clicking and selecting "Turn Over" reverses the order and flips the entire Card Holder.
        Example: A Card Stack with three cards A (face-up), B (face-down) and C (face-down) turned over rearranges the Card Stack to cards C (face-up), B (face-up) and A (face-down).

    Rule: Component Stacks may be shuffled unless restricted.
        Example: Long-Clicking and selecting "Shuffle" shuffles all Cards in the Card Stack

    Rule: Component Stacks may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the entire Component Stack/Holder
    
    Rule: Component Stacks (except for the Holder) may be removed from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes all its Components from the game.
