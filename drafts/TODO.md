# need to determine how x,y,z position of Components is handled once they are placed in a big Stack that doesn't show every one of them
# label for flip (as it can also be "re-roll")?
# we need a Component which is neither Stack nor Spread but may contain Components that are dropped on it and just leave them where they are dropped (possibly also make them disappear like a bag). This Component then could be target or source for automation. Could be used for Hands.

# Ad-hoc stacks allow whatever the Component group allows to do for any individual Component
# Restrictions can only be made for stacks in Component Holders
# Hands are basically Component Holders. The only difference is that each Hand exists once per player. Each game needs to define the abilities/restrictions of each Hand.

# No need for Piles vs. Spreads. Basically, Spreads are just "unpacked" Stacks and we could even allow for Stacks to be toggled between packed and unpacked display, also as a temporary display so you could get a quick look at all cards in a stack.

# Consider Holder Grids, so there's no need to define a hundred Holders for a 10x10 card layout.
