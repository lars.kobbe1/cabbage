Feature: Item Bags
    # TODO: consider if Item Bags are just TokenHolders with Token Stacks (not Spreads), which are invisble until they are moved off the Stack.
    Item Bags are similar to Holders in that they have a drop-zone and can serve as a target (or source) for automation.
    They are similar to Stacks in that they have a Tag to indicate the amount of Items contained and allow for selecting multiple Items to be drawn from them.

    Rule: Item Bags may be moved to another position on the table unless restricted.
        Example: Dragging & dropping the Tag of any Item Bag moves it.

    Rule: Item Bags cannot be rotated.
        #TODO: consider why not

    Rule: Items within Item Bags may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Items (but not the Item Bag) from the game.

    Rule: Item Bags are visible on the table unless defined to be hidden.
    #TODO: should the "drop-zone" display style be configurable?
        Example: Sometimes, the "drop-zones" are not meant to be visible, be it on the Board or in the Hand.

    Rule: Item Bags allow to manually add Items by dropping onto them unless they are restricted.
        Example: Some Item Bags may only allow adding Items via automation.

    Rule: Item Bags may be defined to allow only Items of a certain Type.
        Example: Adding a Dice of type Big to an Item Bag for Items of type Small should not be possible.

    Rule: Dice dropped on an Item Bag are turned invisible.
        Example: Dice dropped on the left-bottom corner of the Item Bag vanish from view.

    Rule: Item Bags allow to manually move Items from them unless they are restricted.
        Example: Some Item Bags may only allow moving Items via automation.

    Rule: Item Bags may have a label, displayed to the L/R/T/B of the Item Bag.
