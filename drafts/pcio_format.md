# PCIO Format Comparison

## Board
    PCIO:
    * id
    * type: "board"
    * x, y, z
    * width & height
    * boardImage
---
    CBBG Definition:
    * id 
    * type: "board"
    * width & height
    * faces:
        * image
    CBBG State:
    * space: Surface
    * position: x, y, z

## Game Piece
    PCIO:
    * id
    * type: "gamePiece"
    * x, y, z
    * dragging
    * color
    * pieceType: "checkers"
---
    CBBG Definition:
    * id 
    * type: "checker"
    * grouping: (color)
    * width & height
    * faces:
        * #1
            * image
    CBBG State:
    * space: Surface
    * position: x, y, z
    * isDraggable: true

## Cards (using an example of a 2-card deck & pile)
    PCIO (card):
    * id
    * x, y, z
    * type: "card"
    * deck: id of deck
    * cardType: id of cardType
    * faceup: false

    PCIO (deck)
    * id
    * type: "cardDeck"
    * x, y, z
    * dragging
    * parent (id of cardPile)
    * label
    * allowPlayerEditLabel
    * cardTypes:
        * id
            * label
            * image
        * ...
    * faceTemplate
        * includeBorder
        * includeRadius
        * objects
            * type: image
            * x,y,w,h
            * color
            * valueType: dynamic
            * value: image
    * backTemplate
        * includeBorder
        * includeRadius
        * objects
            * type: image/text
            * x,y,w,h
            * color
            * valueType: dynamic/static
            * value: image

    PCIO (deck)
    * id
    * type: "cardPile"
    * x, y, z
    * dragging
    * hasShuffleButton
---
    CBBG Definition (card):
    * id 
    * type: "card"
    * width & height
    * border: none/rounded/sharp
    * faces:
        * #0
            * name: "face-up"
            * image
            * zoom
        * #1
            * name: "face-down"
            * image
            * zoom
    * click: flip forward to next face
    * long click: enlarge
    CBBG State (card):
    * space
    * position: x, y, z
    * current face: #2

    CBBG Definition (deck):
    * id:
    * type: "stack"
    CBBG State (deck):
    * space
        * position: x, y, z
    * stackForm: pile/spread
    * components:
        * #0
            * id of card
        * #1
            * id of card
    * selection (number of selected Components within stack)
    * isDragged
    * isPeeked
    * holder
        * id
        * position: x, y
        * isTotalDisplayed
        * isFlippable
        * isTurnable
        * isShuffable
        * isClickable
        * isDraggable

    CBBG Definition (holder):
    * id:
    * type: "holder"
    * isTotalDisplayed (content of holder)
    * isFlippable (content of holder)
    * isTurnable (content of holder)
    * isShuffable (content of holder)
    * isClickable (content of holder)
    * isDraggable (content of holder)

---

* __Hand__
    * id
    * type: "hand"
    * x, y, z
    * dragging
* __Automation Button__
    * id
    * x,y,z
    * type: "automationButton"
    * dragging
    * label: "DEAL"
    * clickRoutine:
        * func: "MOVE_CARDS_BETWEEN_HOLDERS"
        * args:
            * from:
                * type: "literal"
                * value: id of cardPile
            * to:
                * type: "literal"
                * value: id of cardPile
            * quantity:
                * type: "literal"
                * value: (number)
        * func: "CHANGE_COUNTER"
        * args:
            * counters:
                * type: "literal"
                * value: id of counter
            * changeNumber:
                * type: "literal"
                * value: (number)
* __Counter__
    * id
    * type: "counter"
    * x,y,z
    * dragging
    * counterValue: (number)
* __Spinner__
    * id
    * type: "spinner"
    * x,y,z
    * options: (array of numbers)
    * rotation (degree)
    * value (target option)
