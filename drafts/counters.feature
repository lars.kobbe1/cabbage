Feature: Counters
    Counters show a number and can be moved anywhere on the table. They allow to change the number using +/- buttons or through typing a new number.

    # TODO: Do we even need Counters or can they be represented with Tokens an automation buttons?

    Rule: Counters may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Counter moves it.

    Rule: Counters may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the Counter.

    Rule: Counters may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Counter from the game.

    Rule: Couters may be defined to show + and - buttons and their positioning (L-R or T-B)
        Example: A Counter may have +/- buttons at the top and bottom of the Counter.
        Example: A Counter may have no buttons, but simply the (input) field

    Rule: Users may change the value of a Counter unless restricted.
        Example: A Counter may be read-only (from a User's view) and solely controlled by automation.
        Example: Clicking a Counter may allow the User to enter a new value.
