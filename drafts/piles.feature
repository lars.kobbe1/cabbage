Feature: Component Piles
    Piles are drop-zones for Components. They can override abilities of Components, such as not allowing them to be rotated, flipped or reshuffled within the Pile.

    Properties:
    * unique id
    * position: either "Hand" or "Screen" (for "Hand" it means that each player will get their own Pile, each with a different id)
    * restrictions:
        * type of Components that may be added (optional)
        * group of Components that may be added (optional)
    * x, y and z position (relative to either Hand or Screen)
    * width and height
    * label
        * text
        * x offset from top-left corner
        * y offset from top-left corner
        * text rotation
    * initial alignment of dropped Components:
        * x offset from top-left corner (in px or percentage?)
        * y offset from top-left corner (in px or percentage?)
    * alignment of additional Components (relative to first):
        * x offset from bottom-left corner (in px or percentage?)
        * y offset from bottom-left corner (in px or percentage?)
        * stack order: either "above" or "below" (for stacks)
    Abilities/Controls:
    * shows label?
    * shows total of contained Components in Tag? (if hidden, show "?") # TODO: clarify if this is useful as Piles may already have Stacks
    * flip Components when they are added to the Pile?
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are added to the Pile?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * flip Components when they are moved out of the Pile?
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are moved out of the Pile?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * show outline? (# TODO: clarify if we need styles or even images for Piles)
    * turn invisible and non-interactive? (for bags)
    * allows stacking of Components? (not recommended for bags)
        * stack automatically?
        * stack template id (only required for Piles)
        * allows turning over of Components as Stack?
        * allows shuffling of Components as Stack?
    Override Abilities/Controls:
    * Components can be moved out of Pile? (by dragging & dropping)
    * Components can be moved into Pile? (by dragging & dropping)
    * Components can be flipped? (ignored for single-sided Component types)
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * Components can be tossed? (ignored for single-sided Components)
    * Components can be rotated?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * Components can be spun? (shows a pointer that rotates, ignored for single-sided Components)
    * Components can be removed? (from the game)
    * Components can be enlarged?

    Rule: Outlines of Piles are visible unless defined to be hidden.
        Example: Sometimes, the "drop-zones" are not meant to be visible, not even in the Hand.

    Rule: Piles allow to manually add Components by dropping unless they are restricted.
        Example: Some Piles may only allow adding Components via automation.

    Rule: Components dropped on a Pile are re-positioned according to the alignment settings.
        Example: Components dropped on the left-bottom corner of the Pile are repositioned to the center.

    Rule: Piles allow to manually move Components from them unless they are restricted.
        Example: Some Piles may only allow moving Components via automation.

    Rule: Piles in Hands may be defined to show their total of Components in the Avatar tag.
        Example: The Avatar tag of the player with 4 cards in the Card Pile of their Hand shows "4"
        # TODO: consider to add the total in the Avatar tag of each Pile that is set to show its total

    Rule: Piles may have a label, displayed to the L/R/T/B of the Pile.
