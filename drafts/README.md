# CaBbaGE - Card-and-Board-based Game Engine

## What's new or significantly different to PICO?
* Avatars (early concept). For some games, it's important to see the amount of cards on the hand of each player, which PCIO doesn't allow for. The idea for Avatars is to let players chose some icon to represent themselves and show these icons to everyone in the room. A short tag/handle next to each icon shows the number of cards.
* Timers (early concept). For some games, it's important to have a timer that everyone can see. The idea is that a Timer has integrated functions such as start/stop/reset.
* ~~Cards and Pieces~~Components. In PCIO, cards can be used to represent all kinds of things, not just typical playing cards. In CaBbaGE, all these things use the generic term Components (and Template for the common properties defined in a deck). You can think of Cards as two-sided Components which can be stacked and placed on a Holder. The height and width as well as the back side (and many more properties) can be defined in the Template.
* ~~Decks~~Component Types and Component Groups. In PCIO, decks define common properties of cards. They are also used to define which cards belong together, and how many copies exist of each card. In CaBbaGe, you would create a Component type template for cards with common features and you create a Component Group to build a specific card deck.
* Spinners and dice. In PCIO, dice can be represented as spinners, which do not allow to stack. There are games which need dice that can be stacked, so the idea here is that dice can be configured as needed, acting as spinners in some cases and acting like cubes in others.

## Important terms and concepts
* _Room_. The Room is where players can enter/leave and play a game. It's empty (except maybe for a menu) and everything in it is defined by whatever game template is imported.
* _Widget_. These are they custom-definable elements that can be placed inside the Room.
* _(Widget) Template_. Like "instances of classes" in programming, every Widget you see on the screen can be regarded as a unique instance of its Template. The Template defines common properties, such as height and width as well as abilities such as whether players can move, rotate, or flip them.

## Widgets
* _[Hands](hands.feature)_. Every player has their own Hand (or multiple Hands for more complex games), which is (typically) only visible to them. A Hand is a container for Components (and Component Holders).
* _[Avatars](avatars.feature)_. Each player (and their Hand) is represented with an Avatar icon (and an adjacent tag).
* _[Timers](timers.feature)_. Everyone can see the timers counting up (or down).
* _[Counters](counters.feature)_. Everyone can see the counters. They can typically be changed with +/- buttons.
* _Automation Buttons_. Used to configure custom actions such as moving four cards from one place to another.
* _[Components](components.feature)_. There are a variety of Components which are used for the game, typically these are boards, cards, dice, tokens and the like. Some Component types allow for stacking (e.g. cards). Depending on how many sides a Component has, they can represent different things:
    * Single-sided Components (typically used for game boards and figures)
    * Two-sided Components (typically used for cards)
    * Multi-sided Components (typically used for dice and spinners)
* _[Component Groups](componentGroups.feature)_. A Component Group is a virtual collection of individual Components. They are typically used to define a list of cards that belong to a particular card deck, but are also very useful to work with as pre-defined targets for automation.
* _Component Holders_. A Component Holder is a "drop-zone" that other Components can dragged & dropped on (such as for discarding of cards). There are two types of Component Holders:
    * _[Spreads](spreads.feature)_. Components ins Spreads are re-arranged so they overlap partially (or don't overlap), so players can see and interact with every individual Component in the Spread. They are typically used for cards in the player's hand, but can equally well be used to represent a player's cup of dice.
    * _[Piles](piles.feature)_. Components dropped in Piles are not re-arranged, other than stacking them on top of already present Components of the same type. They can contain any amount of Stacks and individual Components of any type, but are typically used to simply hold a single deck of cards as a Stack.

*= Components can be defined to automatically stack when dropped on each other. Thereby, they form an ad-hoc Component Stack.
