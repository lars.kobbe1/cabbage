Feature: Component Spreads

    Component Spreads are drop-zones for Components. In a Spread, each Component is individually visible (at least partially) and may be selected for subsequent action, such as to be re-rolled or to be moved in bulk. Like Stacks, Spreads have a tag to show the number of currently selected Components and the total of contained Components.

    Properties:
    * unique id
    * position: either "Hand" or "Screen" (for "Hand" it means that each player will get their own Component Spread, each with a different id)
    * restrictions:
        * type of Components that may be added (required)
        * group of Components that may be added (optional)
    * x, y and z position (relative to either Hand or Screen)
    * width and height
    * label
        * text
        * x offset from top-left corner
        * y offset from top-left corner
    * initial alignment of dropped Components:
        * x offset from top-left corner (in px or percentage?)
        * y offset from top-left corner (in px or percentage?)
    * alignment of additional Components (relative to first):
        * x offset from bottom-left corner (in px or percentage?)
        * y offset from bottom-left corner (in px or percentage?)
        * stack order: either "above" or "below"

    # TODO: definition for what happens if there is more than a safe amount of Components in the Spread
    Abilities/Controls:
    * shows label
    * shows total of contained Components in Tag? (if hidden, show "?")
    * flip Components when they are added to the Spread?
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are added to the Spread?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * flip Components when they are moved out of the Spread?
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * rotate Components when they are moved out of the Spread?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * show outline? (# TODO: clarify if we need styles or even images for Spreads)
    Override Abilities/Controls:
    * Components can be moved out of Spread? (by dragging & dropping)
    * Components can be moved into Spread? (by dragging & dropping)
    * Components can be flipped? (ignored for single-sided Component types)
        * if yes, flips to a) next/other side or b) random side or c) choice of side
    * Components can be tossed? (ignored for single-sided Components)
    * Components can be rotated?
        * if yes, rotates to a) next/other segment or b) random segment or c) choice of segment
    * Components can be spun? (shows a pointer that rotates, ignored for single-sided Components)
    * Components can be removed? (from the game)
    * Components can be enlarged?

    Rule: Spreads always restrict their contained Components to a specific, pre-determined type.

    Rule: Spreads allow to define the "spread" direction (L/R/T/B) of Components.
        Example: When direction is set to "right", subsequent Components are placed to the right of the previous Component.
    
    Rule: Spreads allow define a "selection zone" (L/R/T/B), which must be perpendicular to the "spread" direction.
        Example: Selected Components are displayed slightly off the other Components to make them stand out.

    Rule: Spreads allow to define the overlap and alignment point of spreaded Components.
    # TODO: Figure out the exact rules for ensuring that overlap works well with spread direction and selection zone.
        Example: When overlap is 10px to top-left and the spread direction is "right", Component 2 is placed 10 px below the top-left of Component 1.
        Example: When overlap is 50% right of middle-center, Component 2 is placed 50% below the middle-center of Component 1.
    
    Rule: The stack order of Components can be set to "above" or "below" the other Components.
        Example: When stack order is set to "above", additional Components are placed above the previous Component in the spread. Without a visible overlap, this cannot be seen.

    Rule: The default side of Components can be set to any side or "keep" for Spreads.
        Example: When the default side is set to "up" (1), additional Components are placed face-up on the stack.
        Example: When the default side is set to "keep", the current side of additional Components is retained when being placed on the stack.
    
    Rule: Components in a Spread may be selected. Components default to "unselected".
        Example: Dragging & dropping a Component into the "select zone" of the Spread (in a perpendicular direction and still within the Component Holder) selects a Component.
        Example: Dragging & dropping a Component back into the Spread unselects it.

    Rule: Components in a Spread may be moved individually or in bulk unless restricted.
        Example: Dragging & dropping any of the selected Component(s) out of the Component Holder moves all of the selected Component(s).
        Example: Dragging & dropping the tag moves the selected Component(s) to somewhere else. While being dragged, the Components are considered to be ad-hoc stacked for all purposes of logic and animation.

    Rule: Components in a Spread may be individually moved to another position in the Component Holder.
        Example: The order of Components may be re-arranged by dragging & dropping individual Components to another position.

    Rule: Components in a Spread may be flipped individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Flip" flips the selected Card(s) in the Spread.
        Example: Long-Clicking and selecting "Re-Roll" re-rolls the selected Dice(s) in the Spread.

    Rule: Components in a Spread may be rotated individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the selected Component(s) in the Spread.
    
    Rule: Components in a Spread may be removed individually or in bulk from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the selected Component(s) from the game.
