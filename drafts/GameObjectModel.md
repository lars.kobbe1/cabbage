# Game Object Model (GOM):
There are three levels of container nodes inside a Room:
* Spaces (Surface / Hand / Limbo)
    * Holders ("drop-zones")
        * Stacks

Components can be placed at any level:
* Surface(s) (shared space)
    * Component(s)
    * Stack(s)
        * Component(s)
    * Holder(s)
        * Stack(s)
            * Component(s)
        * Component(s)
* Hand(s) (individual space per player, can optionally be shared)
    * Holder(s)
        * Stack(s)
            * Component(s)
        * Component(s)
* Limbo (space for Components removed from the game, typically until the game is reset)
    * Component(s)

Holders, Stacks, and Components can be moved around but may only exist at any one node at a time.

Each container level has a different dimensionality:
* Space (3D). A Component, Stack or Holder is located at x,y and z position relative to the Space. The Space position is translated to the position of the image on the screen.
* Holder (2D). A Holder has a 1x1 grid of "drop-zones", i.e. a single drop-zone, but the grid can be extended up to 64x64, meaning that any Stack or individual Component can be located at different positions in the Holder.
* Stack (1D). Components are located at a particular position in the stack (starting at 0). The Stack position is translated to a z-position on screen, so that the one with the highest position in the stack also has the highest z index on screen.

The GOM is populated from a Game Module (.cbbg or .pcio file). The .cbbg package contains a number of files:
* Definitions
* Initial Game State
* Assets (unless hosted somewhere else)

Components have Definitions and States. Components are created via import of a Game Module (.cbbg or .pcio file).
    * Definitions describe the default properties and abilities.
    * States describe the current properties and abilities, as well as the current Shape.

Two kinds of Containers: Stacks and Holders
    * Holder can be empty and override Definitions.
    * Stacks are created, updated and removed as needed.
