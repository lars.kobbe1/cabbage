Feature: Card Piles
    Card Piles are drop-zones for Cards on the table. They are similar to Card Stacks in that they contain any number of Cards, but they allow Cards to be arranged as a Card Spread instead of a Card Stack. 
    They may also serve as a target (or source) for automation (in contrast to Card Stacks, which are incidental).
    When placed in Hands, they are typically of type Card Spread.

    Rule: Card Piles are visible on the table unless defined to be hidden.
    #TODO: should the "drop-zone" display style be configurable?
        Example: Sometimes, the "drop-zones" are not meant to be visible, be it on the Board or in the Hand.

    Rule: Card Piles allow to manually add cards by dropping onto them unless they are restricted.
        Example: Some Card Piles may only allow adding Cards via automation.

    Rule: Card Piles may be defined to allow only Cards of a certain Type.
        Example: Adding a Card of type Big to a Card Pile for Cards of type Small should not be possible.

    Rule: Cards dropped on a Card Pile are re-positioned according to the Card Pile layout settings.
        Example: Cards dropped on the left-bottom corner of the Card Pile are repositioned to the center.

    Rule: Card Piles allow to manually move cards from them unless they are restricted.
        Example: Some Card Piles may only allow moving Cards via automation.

    Rule: Card Piles are defined to be either of type "Card Stack" or "Card Spread".

    Rule: Card Piles may have a label, displayed to the L/R/T/B of the Card Pile.

    Rule: Card Piles in Hands may be defined to show their total of Cards in the Avatar tag.
        Example: The Avatar tag of the player with 4 cards in the Card Pile of their Hand shows "4"
    # TODO: consider if this applies to other Piles as well and if there could be multiple Piles in a Hand
