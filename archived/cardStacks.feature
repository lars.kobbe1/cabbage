Feature: Card Stacks
    Card Stacks in Card Piles have additional functionality to be explained here.
    
    Rule: Cards may be moved together when dragging and dropping their Tag.
        Example: A Card Stack with 5 Cards is moved by using the "5" Tag instead of the top-most Card.

    Rule: Card Stacks show the number of contained Cards unless the total should be hidden.
        Example: When no total is desired, the Tag shows "?".

    Rule: The stacking order of Cards is set to either "above" or "below" the other Cards.
        Example: When stacking order is set to "above", added cards are placed above the previous Card in the stack.

    Rule: The default side of Cards is set to either "up", "down" or "keep" for stacked Cards.
        Example: When the default side is set to "up", added Cards are placed face-up on the stack.
        Example: When the default side is set to "keep", the current side of added Cards is retained when being placed on the stack.

    Rule: The amount of visibly stacked Cards can be defined.
        Example: When only 3 Cards are defined to be visible, adding a fourth Card changes the Tag count, but not the visible presentation of stacked Cards.

    Rule: The overlap of Cards in the Card Stack can be defined.
        #TODO: clarify where this is defined (perhaps in a Card Template?)

    Rule: The top-most Card may be flipped on the Card Pile unless restricted.
        Example: Clicking the top-most card flips a face-down Card to its face-up side.

    Rule: Card Stacks may be flipped unless restricted.
        Example: Long-Clicking and selecting "Flip" flips all Cards in the Card Stack

    Rule: Card Stacks may be turned over unless restricted.
        Example: Long-Clicking and selecting "Turn Over" reverses the order and flips the entire Card Pile.
        Example: A Card Stack with three cards A (face-up), B (face-down) and C (face-down) turned over rearranges the Card Stack to cards C (face-up), B (face-up) and A (face-down).

    Rule: Card Stacks may be shuffled unless restricted.
        Example: Long-Clicking and selecting "Shuffle" shuffles all Cards in the Card Stack

    Rule: Card Stacks may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the entire Card Pile
    
    Rule: Card Stacks (except for the Pile) may be removed from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes all its Cards from the game.
