Feature: Cards
    Cards can be moved anywhere on the table. Cards of the same type can be stacked together in a Card Stack.

    Rule: Any Card may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Card moves it.

    Rule: Moving a Card onto another Card of the same type creates a Card Stack.
        Example: Dragging & dropping a Card of type BigCard on another Card of type BigCard makes them stack together.
        Example: Dragging & dropping a Card of type BigCard on another Card of type BigCard simply moves the big Card.

    # TODO: Shouldn't Cards always stack with Cards of same type? Perhaps with generic Components, this can be determined.
    # Rule: Cards may be defined to not stack.

    Rule: Cards may be flipped unless restricted.
        Example: Clicking a face-down Card flips it face-up.

    Rule: Cards may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the Card.

    Rule: Cards may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Card from the game.
    
    Rule: Cards may be tossed to a random side unless restricted.
        Example: Long-Clicking and selecting "Toss" flips the Card to a random side.

    Rule: Cards may be enlarged. The sides to be enlarged can be defined.
        Example: Hovering over a face-up Card shows an enlarged version of the Card.
