Feature: 
    Dice are basically multi-sided Tokens that may be randomly flipped to any side. Like Spinners, they could be animated to appear spinning until they point to a segment, which is then displayed clearly. Each side/segment may have a number/name/image.

    Rule: Dice may be moved to another position on the table.
        Example: Dragging & dropping any Dice moves it.

    Rule: Dice may be rolled/spun/flipped unless restricted.
        Example: Clicking a Dice rolls it until it shows a side.
        Example: Clicking a Dice spins it until it shows a segment.

    Rule: Moving a Dice onto another Dice of the same type creates a Dice Stack unless restricted.
        Example: Dragging & dropping a Dice of type Big on another Dice of type Big makes them stack together.
    
    Rule: Any Dice may be defined to not stack.
        #TODO: consider if that's an unstackable "Type" or another boolean.

    Rule: Dice cannot rotate.
    #TODO: consider if that's an unrotatable "Type" or another boolean.

    Rule: Dice may be set to a particular side (segment).
        Example: Long-Clicking and selecting an option makes the Dice show to the selected side (segment).

    Rule: Dice may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Dice from the game.
