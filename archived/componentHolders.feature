Feature: Component Holders
    Component Holders are drop-zones for Components. They may also serve as a target (or source) for automation.
    They typically re-arrange and Components dropped into them and can override their abilities, such as not allowing them to be rotated, flipped or reshuffled.

    Properties:
    * unique id
    * type: "Spread" or "Pile". The type determines whether and how Components will be re-arranged automatically when added.
    * position: either "Hand" or "Screen" (for "Hand" it means that each player will get their own Component Holder, each with a different id)
    * restrictions:
        * type of Components that may be added (optional)
        * group of Components that may be added (optional)
    * x, y and z position (relative to either Hand or Screen)
    * width and height
    * label
        * text
        * x offset from top-left corner
        * y offset from top-left corner
    * alignment of dropped Components:
        * x offset from top-left corner (in px or percentage?)
        * y offset from top-left corner (in px or percentage?)
    Abilities/Controls:
    * shows label? (unless in a Stack or Holder)
    * show outline? (# TODO: clarify if we need styles or even images for Component Holders)
    * shows total in Avatar tag?
    * can be referenced as target or source by automation? (ignored for Component Holders in Hand)
    * allows manual adding of Components? (via drag & drop)
    * allows manual removal of Components? (via drag & drop)
    * allows turning over of Components as Stack?
    * allows shuffling of Components as Stack?
    * allows stacking of Components?
        * stack automatically?
        * stack template id (only required for Piles) 

    Rule: Outlines of Component Holders are visible unless defined to be hidden.
        Example: Sometimes, the "drop-zones" are not meant to be visible, not even in the Hand.

    Rule: Component Holders allow to manually add Components by dropping unless they are restricted.
        Example: Some Component Holders may only allow adding Components via automation.

    Rule: Components dropped on a Component Holder are re-positioned according to the Component Holder layout settings.
        Example: Components dropped on the left-bottom corner of the Component Holder are repositioned to the center.

    Rule: Component Holders allow to manually move Components from them unless they are restricted.
        Example: Some Component Holders may only allow moving Components via automation.

    Rule: Component Holders in Hands may be defined to show their total of Components in the Avatar tag.
        Example: The Avatar tag of the player with 4 cards in the Card Holder of their Hand shows "4"
        # TODO: consider to add the total in the Avatar tag of each Holder that is set to show its total

    Rule: Component Holders may have a label, displayed to the L/R/T/B of the Component Holder.
