Feature: Dice Stacks
    
    Rule: Dice may be moved together when dragging and dropping their Tag.
        Example: A Dice Stack with 5 Dice is moved by using the "5" Tag instead of the top-most Dice.

    Rule: Dice Stacks show the number of contained Dice unless the total should be hidden.
        Example: When no total is desired, the Tag shows "?".
        # TODO: consider if that makes sense for dice

    Rule: The stacking order of Dice is set to either "above" or "below" the other Dice.
        Example: When stacking order is set to "above", added Dice are placed above the previous Dice in the stack.

    Rule: The default side of Dice is set to any side or "keep" for stacked Dice.
        Example: When the default side is set to "1", added Dice are placed with side "1" on the stack.
        Example: When the default side is set to "keep", the current side of added Dice is retained when being placed on the stack.
        # TODO: consider if that makes sense for dice

    Rule: The amount of visibly stacked Dice can be defined.
        Example: When only 3 Dice are defined to be visible, adding a fourth Dice changes the Tag count, but not the visible presentation of stacked Dice.
        # TODO: consider if that makes sense for dice

    Rule: The overlap of Dice in the Dice Stack can be defined.
        #TODO: clarify where this is defined (perhaps in a Dice Template?)

    Rule: The top-most Dice may be flipped on the Dice Pile unless restricted.
        Example: Clicking the top-most Dice flips a face-down Dice to its face-up side.

    Rule: Dice Stacks may be flipped unless restricted.
        Example: Long-Clicking and selecting "Flip" flips all Dice in the Dice Stack

    Rule: Dice Stacks may be shuffled unless restricted.
        Example: Long-Clicking and selecting "Shuffle" shuffles all Dice in the Dice Stack

    Rule: Dice Stacks may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the entire Dice Pile
    
    Rule: Dice Stacks (except for the Pile) may be removed from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes all its Dice from the game.
