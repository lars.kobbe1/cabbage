Feature: Tokens
    Tokens have an image and can be moved anywhere on the table. Tokens of the same type can be stacked together in a Token Stack. Unlike Cards, however, Tokens have no sides to be flipped and cannot be shuffled (as they are identical). They also have no "Deck" which defines their common properties.
    
    For game elements that have only a single side but should otherwise behave like Cards, use Card Decks that don't allow flipping.

#TODO: What makes a Token different to a Card? Having a single side or being identical or having no Deck with common properties? For Camel Up, Tokens could represent the camels, but would need to be different in at least their color.

    Rule: Tokens may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Token moves it.

    Rule: Moving a Token onto another Token of the same type creates a Token Stack unless restricted.
        Example: Dragging & dropping a Token of type Big on another Token of type Big makes them stack together.
    
    Rule: Tokens may be defined to not stack.
        #TODO: consider if that's an unstackable "Type" or another boolean.

    Rule: Tokens may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the Token.

    Rule: Tokens may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Token from the game.
    
    #TODO: consider action for clicking a Token (one-sided tokens cannot be flipped)
