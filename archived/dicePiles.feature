Feature: Dice Piles
    Dice Piles are drop-zones for Dice on the table. They are similar to Dice Stacks in that they contain any number of Dice, but they allow Dice to be arranged as a Dice Spread instead of a Dice Stack. They may also serve as a target (or source) for automation.
    Dice Piles allow to be targeted by automation in contrast to Dice Stacks, which are incidental.
    When placed in Hands, they are typically of type Dice Spread.

    Rule: Dice Piles are visible on the table unless defined to be hidden.
    #TODO: should the "drop-zone" display style be configurable?
        Example: Sometimes, the "drop-zones" are not meant to be visible, be it on the Board or in the Hand.

    Rule: Dice Piles allow to manually add Dice by dropping onto them unless they are restricted.
        Example: Some Dice Piles may only allow adding Dice via automation.

    Rule: Dice Piles may be defined to allow only Dice of a certain Type.
        Example: Adding a Dice of type Big to a Dice Pile for Dice of type Small should not be possible.

    Rule: Dice dropped on a Dice Pile are re-positioned according to the Dice Pile layout settings.
        Example: Dice dropped on the left-bottom corner of the Dice Pile are repositioned to the center.

    Rule: Dice Piles allow to manually move Dice from them unless they are restricted.
        Example: Some Dice Piles may only allow moving Dice via automation.

    Rule: Dice Piles are defined to be either of type "Dice Stack" or "Dice Spread".

    Rule: Dice Piles may have a label, displayed to the L/R/T/B of the Dice Pile.
