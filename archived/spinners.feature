Feature: Spinners
    Spinners are basically multi-sided Tokens that may be randomly flipped to any side. Instead, their sides are called "segments" and they are animated to appear spinning until they point to a segment, which is then displayed clearly. Each segment may have a number/name/image.

    Rule: Spinners may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Spinner moves it.

    Rule: Spinners may be spun (flipped) unless restricted.
        Example: Clicking a Spinner spins it until it shows a segment.

    Rule: Spinners may be defined with an undefined starting segment.
        Example: At the start of the game, a Spinner indicates that it has not been spun yet.
        # TODO: clarify if that's really necessary as it's different from how real game components work and is inconsistent with Dice
    
    Rule: Spinners cannot stack.
        #TODO: consider if that's an unstackable "Type" or another boolean.

    Rule: Spinners cannot rotate.
    #TODO: consider if that's an unrotatable "Type" or another boolean.

    Rule: Spinners may be set to a particular segment (side).
        Example: Long-Clicking and selecting an option makes the Spinner point to the selected segment.

    Rule: Spinners may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Spinner from the game.
