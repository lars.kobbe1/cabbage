Feature: Pieces
    Pieces have an image and can be moved anywhere on the table.

    # TODO: Do we even need Pieces or can they be represented with Tokens that don't allow stacking?

    Rule: Pieces may be moved to another position on the table unless restricted.
        Example: Dragging & dropping any Piece moves it.

    Rule: Pieces may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the Piece.

    Rule: Pieces may be removed unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the Piece from the game.
