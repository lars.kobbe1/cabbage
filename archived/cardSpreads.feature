Feature: Card Spreads

    Card Spreads can only be used as part of a Card Pile.
    They differ from Card Stacks in that they have a variable display size and each Card is individually visible (at least to some degree) and may potentially be selected, for instance to be moved (as Card Spreads do not use Tags for moving Cards together) or targeted in an automation.

    Rule: Card Spreads allow to define the "spread" direction (L/R/T/B) of Cards.
        Example: When direction is set to "right", subsequent cards are placed to the right of the previous Card.
    
    Rule: Card Spreads allow define a "selection zone" (L/R/T/B), which must be perpendicular to the "spread" direction.
        Example: Selected Cards are displayed slightly off the other Cards to make them stand out.

    Rule: Card Spreads allow to define the overlap and alignment point of spreaded Cards.
    # TODO: Figure out the exact rules for ensuring that overlap works well with spread direction and selection zone.
        Example: When overlap is 10px to top-left and the spread direction is "right", Card 2 is placed 10 px below the top-left of Card 1.
        Example: When overlap is 50% right of middle-center, Card 2 is placed 50% below the middle-center of Card 1.
    
    Rule: The stack order of Cards can be set to "above" or "below" the other Cards.
        Example: When stack order is set to "above", additional cards are placed above the previous Card in the spread. Without a visible overlap, this cannot be seen.

    Rule: The default side of Cards can be set to "up", "down" or "keep" for Card Spreads.
        Example: When the default side is set to "up", additional Cards are placed face-up on the stack.
        Example: When the default side is set to "keep", the current side of additional Cards is retained when being placed on the stack.
    
    Rule: Cards in a Card Spread may be selected. Cards default to "unselected".
        Example: Dragging & dropping a Card into the "select zone" of the Card Spread (in a perpendicular direction and still within the Card Pile) selects a Card.
        Example: Dragging & dropping a Card back into the Card Spread unselects it.

    Rule: Cards in a Card Spread may be moved individually or in bulk unless restricted.
        Example: Dragging & dropping any of the selected Card(s) out of the Card Pile moves the selected Card(s).

    Rule: Cards in a Card Spread may be moved to another position in the Card Pile.
        Example: The order of Cards may be re-arranged by dragging & dropping a Card to another position.

    Rule: Cards in a Card Spread may be flipped individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Flip" flips the selected Card(s) in the Card Spread.

    Rule: Cards in a Card Spread may be rotated individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the selected Card(s) in the Card Spread.
    
    Rule: Cards in a Card Spread may be removed individually or in bulk from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the selected Card(s) from the game.
