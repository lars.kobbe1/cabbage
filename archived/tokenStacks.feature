Feature: Token Stacks

    Rule: Token Stacks define the type of Tokens that can be stacked.
        Example: A token of type A cannot be stacked with a Token of type B.
    
    Rule: Tokens may be moved together when dragging and dropping their Tag.
        Example: A Token Stack with 5 Tokens is moved by using the "5" Tag instead of the top-most Token.

    Rule: Token Stacks show the number of contained Tokens unless the total should be hidden.
        Example: When no display is desired, the Tag shows "?".

    Rule: Token Stacks show the number of selected Tokens.
        Example: When two out of 5 Tokens are selected, the Tag shows "2/5".
        Example: When no total display is desired, the Tag shows "2/?".
    
    Rule: The Token selection can be undone in a single step.
        Example: Long-Clicking the Tag unselects all Tokens.

    Rule: Selected Tokens may be moved together when dragging and dropping their Tag.
        Example: A Token Stack with 2 out of 5 Tokens selected is moved by using the "2/5" Tag instead of the top-most Token.

    Rule: The stacking order of Tokens is set to either "above" or "below" the other Tokens.
        Example: When stacking order is set to "above", added Tokens are placed above the previous Token in the stack.
    
    Rule: The amount of visibly stacked Tokens can be defined.
        Example: When only 3 Tokens are defined to be visible, adding a fourth Token changes the Tag count, but not the visible presentation of stacked Tokens.
    
    Rule: The overlap of Tokens in the Token Stack can be defined.
        #TODO: clarify where this is defined (perhaps in a Token Template?)

    Rule: Tokens may be selected individually or in bulk. By default, Tokens are unselected.
        Example: Clicking the Token Stack increases the selected count in the Tag.

    Rule: Token Stacks may be rotated unless restricted.
        Example: Long-Clicking and selecting "Rotate" rotates the entire Token Pile
    
    Rule: Token Stacks (except for the Pile) may be removed from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes all its Tokens from the game.
