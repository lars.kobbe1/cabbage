Feature: Dice Spreads

    Dice Spreads can only be used as part of a Dice Pile.
    They differ from Dice Stacks in that they have a variable display size and each Dice is individually visible (at least to some degree) and may potentially be selected, for instance to be moved (as Dice Spreads do not use Tags for moving Dice together) or targeted in an automation.

    Rule: Dice Spreads allow to define the "spread" direction (L/R/T/B) of Dice.
        Example: When direction is set to "right", subsequent Dice are placed to the right of the previous Dice.
    
    Rule: Dice Spreads allow define a "selection zone" (L/R/T/B), which must be perpendicular to the "spread" direction.
        Example: Selected Dice are displayed slightly off the other Dice to make them stand out.

    Rule: Dice Spreads allow to define the overlap and alignment point of spreaded Dice.
    # TODO: Figure out the exact rules for ensuring that overlap works well with spread direction and selection zone.
        Example: When overlap is 10px to top-left and the spread direction is "right", Dice 2 is placed 10 px below the top-left of Dice 1.
        Example: When overlap is 50% right of middle-center, Dice 2 is placed 50% below the middle-center of Dice 1.
    
    Rule: The stack order of Dice can be set to "above" or "below" the other Dice.
        Example: When stack order is set to "above", additional Dice are placed above the previous Dice in the spread. Without a visible overlap, this cannot be seen.

    Rule: The default side of Dice can be set to any side or "keep" for Dice Spreads.
        Example: When the default side is set to "1", added Dice are placed with their "1" side up on the stack.
        Example: When the default side is set to "keep", the current side of added Dice is retained when being placed on the stack.
    
    Rule: Dice in a Dice Spread may be selected. Dice default to "unselected".
        Example: Dragging & dropping a Dice into the "select zone" of the Dice Spread (in a perpendicular direction and still within the Dice Pile) selects a Dice.
        Example: Dragging & dropping a Dice back into the Dice Spread unselects it.

    Rule: Dice in a Dice Spread may be moved individually or in bulk unless restricted.
        Example: Dragging & dropping any of the selected Dice(s) out of the Dice Pile moves the selected Dice(s).

    Rule: Dice in a Dice Spread may be moved to another position in the Dice Pile.
        Example: The order of Dice may be re-arranged by dragging & dropping a Dice to another position.

    Rule: Dice in a Dice Spread may be re-rolled individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Re-Roll" re-rolls the selected Dice(s) in the Dice Spread.

    Rule: Dice in a Dice Spread may be flipped to a particular side individually or in bulk unless restricted.
        Example: Long-Clicking and selecting "Flip" flips the selected Dice(s) to a particular side in the Dice Spread.
        # TODO: consider whether dice flip to their "next" side or to their "first" (and then next) side (according to the Dice Template)

    Rule: Dice in a Dice Spread cannot be rotated.
    
    Rule: Dice in a Dice Spread may be removed individually or in bulk from the game unless restricted.
        Example: Long-Clicking and selecting "Remove" removes the selected Dice(s) from the game.
