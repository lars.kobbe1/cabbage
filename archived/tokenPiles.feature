Feature: Token Piles
    Token Piles are drop-zones for Tokens on the table. They may also serve as a target (or source) for automation.

    Rule: Token Piles are visible on the table unless defined to be hidden.
    #TODO: should the "drop-zone" display style be configurable?
        Example: Sometimes, the "drop-zones" are not meant to be visible, be it on the Board or in the Hand.

    Rule: Token Piles allow to manually add Tokens by dropping unless they are restricted.
        Example: Some Token Piles may only allow adding Tokens via automation.

    Rule: Token Piles may be defined to allow only Tokens of a certain Type.
        Example: Adding a Token of type Big to a Token Pile for Tokens of type Small should not be possible.

    Rule: Tokens dropped on a Token Pile are re-positioned according to the Token Pile layout settings.
        Example: Tokens dropped on the left-bottom corner of the Token Pile are repositioned to the center.

    Rule: Token Piles allow to manually move Tokens from them unless they are restricted.
        Example: Some Token Piles may only allow moving Tokens via automation.

    Rule: Token Piles may have a label, displayed to the L/R/T/B of the Token Pile.
