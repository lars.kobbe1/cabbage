Feature: Shapes
    Shapes are the virtual representation of Components on the screen (in video games, they would be called "Sprites"). Components do not need a Shape to exist (they may be "off-screen", in a bag or in a Stack that's too large to show all Components), but without Shape they cannot be interacted with by players via touch/click and drag.
    
    Shapes are used for:
    * Individual Component
    * Stack of Components
    * Component Holder (drop-zone with visible outline)

    Every Shape has an Owner, i.e. a parent to which it is the child. For a single card lying on the Surface, the Surface is its Owner. For a Stack of twenty cards lying on the Surface, the Surface is the Owner of the Stack. The Stack is also the Owner of each of the twenty cards.

    The Stack Template defines how many contained components are visually represented on screen. A Stack which shows the 3 top-most cards consists of 4 Shapes, one Shape for each of the visible cards and another for the Tag next to the stack of cards.

    Properties:
    * Shape ID
    * type: Component, Stack or Holder
        * ID (of Component, Stack or Holder)
    * owner: Surface ID or Hand ID
    * position: x, y and z position (on screen)
    * side
    * rotation segment
    * is visible?
    * is interactive? (i.e. can be clicked or dragged)
    * is stacked? (only for Components)
        * stack ID
