# CaBbaGE - Card-and-Board-based Game Engine

## What's new or significantly different to PICO?
In PCIO, there are a variety of widgets, each with their own special properties and abilities. In CaBbaGE, nearly everything is simply a "Component" (or a container) at the core and allows to be "specialized" via configuration, so that one Component may look & behave like a card, another like a die, and yet another as a game board.

I am going to try illustrate the concept of Components by refering to them simply as "Cards", even though Components may just as well represent six-sided dice.

In PCIO, cards are organized in decks. A deck defines the common properties of all cards in the deck (such as width & height). Decks also assume that these cards somehow belong together and start out stacked together on the game table (so they can later be recalled for a new game). Let's say you had a blue deck of cards and a red deck of cards, each with 52 standard playing cards.

In CaBbaGE, you define a Component type (e.g. "playing card") with common properties (such as width & height) and then define a set of individual Cards of type "playing card". The type is important for determining which Cards can stack or not (because Cards of vastly different shapes would not stack nicely anyhow). You also define Groups that allow to refer to a particular set of Cards. For instance, the "Ace of Spades" card with a red back side could be in the "Aces", "Spades" as well as "Red Deck" Group.

In PCIO, cards can be contained either by Hands (typically arranged as a spread of partially overlapping cards) or by Card Holders (arranged as a stack of cards). In CaBbaGE, cards can be arranged with or without overlap on the game table as well as in a hand. For instance, the main Hand could be a Card Holder showing cards in a "Spread" (with partial overlap) whereas an extra Hand could show cards in a "Pile" (full overlap). The game table could show cards arranged as a single-row "Grid" (with no or "negative" overlap aka "padding") and another area with Cards arranged as a multi-row "Grid".

The difference between a Holder and a Hand is that a Hand is a container on a higher level, so for instance a Hand may contain multiple Holders or a combination of Holders, Stacks and individual Components.

Here's some sketches to convey the concept of Components, Stacks and Holders:

![View Sketches](sketches.svg)
