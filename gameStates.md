# Game States

Initial Game States are loaded when importing a Game Module. They are controlled centrally by the server, so clients can send change requests, but it's up to the server to accept them.

## Component Properties:
* Current Space name and position (x,y,z). The Space (position) in which the Component is located. References a particular Surface, Hand or the Limbo.
* Current Holder name and position (x,y). The Holder (position) in which the Component is located. Empty if Component is not in any Holder.
* Current Stack ID and position (z). The Stack (position) in which the Component is located. Empty if Component is not in any Stack.
* Current Face (name of current face).
* Current Turn (name of current turn).
* Selected (on/off). Describes whether the Component is currently in a selected state, so other players are aware.
* Clickable (on/off). Component does not react to clicking unless enabled.
* Draggable (on/off). Component does not react to dragging unless enabled.
* isDragged? Describes whether the Component is currently dragged (and by whom?), so other players are aware. Should disable Clickable and Draggable.

## Stack Properties:
* Current Space name and position (x,y,z). The Space (position) in which the Stack is located. References a particular Surface, Hand or the Limbo.
* Current Holder name and position (x,y). The Holder (position) in which the Stack is located. Empty if Stack is not in any Holder. Derived states from Holder definition (otherwise all true/up/keep):
    * stackDirection
    * isTotalDisplayed?
    * isFlippable?
    * FlipOnEnter
    * FlipOnLeave
    * isTurnable?
    * TurnOnEnter
    * TurnOnLeave
    * isClickable?
    * isDraggable?
* Selected (number). Describes the currently selected number of Components in the Stack, so other players are aware.
* isDragged? Describes whether the Stack is currently dragged (and by whom?), so other players are aware. Should disable Clickable and Draggable.
* isPeeked? Describes whether the Stack is currently peeked at (and by whom?), so other players are aware.
* StackForm (Pile/Spread)? Describes whether the Stack is currently using the Spread or Pile form.

## Holder Properties:
* Label.
* isEditable? Describes whether the Holder label is editable.
